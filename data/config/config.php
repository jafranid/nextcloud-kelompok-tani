<?php
$CONFIG = array (
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'oc1ehs1irils',
  'passwordsalt' => 'ZvVYmOmfZtDNUuASyDovNg4gxm8mPP',
  'secret' => '69s9YMa0w578xGLm+IsZLpJ2aV0nTCzQvuuI1MkdfePmFXk0',
  'trusted_domains' => 
  array (
    0 => 'localhost:8080',
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'mysql',
  'version' => '27.0.0.8',
  'overwrite.cli.url' => 'http://localhost:8080',
  'dbname' => 'nextcloud_j',
  'dbhost' => 'db',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => '12345678',
  'installed' => true,
);
